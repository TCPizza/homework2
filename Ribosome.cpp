#include "Ribosome.h"


Protein * Ribosome::create_protein(std::string & RNA_transcript) const
{
	std::string RNA = RNA_transcript;
	Protein* ans = new Protein;
	ans->init();
	bool flag = 1;
	std::string temp = "";
	while (RNA.length() >= 3 && flag)
	{
		temp = RNA.substr(0, 3);
		if (get_amino_acid(temp) != UNKNOWN)
		{
			ans->add(get_amino_acid(temp));
			RNA = RNA.substr(3, RNA.length() - 3);
		}
		else
		{
			std::cerr << "No such amino acid" << std::endl;
			delete(ans);
			ans = nullptr;
			flag = 0;
		}
	}
	return ans;
}
